package com.e_commerce.sell.service.impl;

import com.e_commerce.sell.domain.Invoice;
import com.e_commerce.warehouse.domain.Good;
import com.e_commerce.warehouse.service.WarehouseService;
import com.e_commerce.sell.service.SellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class DefaultSellService implements SellService {
    @Autowired
    private WarehouseService warehouseService;

    @Override
    public Invoice sellGood(String buyerName, String goodUid) {
        Good good = warehouseService.removeGood(goodUid);

        Invoice invoice = new Invoice();
        invoice.setBuyerName(buyerName);
        invoice.setGood(good);
        invoice.setDate(new Date());

        return invoice;
    }
}
