package com.e_commerce.sell.service;

import com.e_commerce.sell.domain.Invoice;

public interface SellService {
    Invoice sellGood(String buyerName, String goodUid);
}
