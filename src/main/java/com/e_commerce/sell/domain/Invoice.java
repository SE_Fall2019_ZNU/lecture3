package com.e_commerce.sell.domain;

import com.e_commerce.warehouse.domain.Good;

import java.util.Date;

public class Invoice {
    private String buyerName;
    private Date date;
    private Good good;

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }
}
