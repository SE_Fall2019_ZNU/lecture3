package com.e_commerce.warehouse.domain;

import java.util.concurrent.atomic.AtomicInteger;

public class Good {
    private static AtomicInteger prevGoodUid = new AtomicInteger();

    private String uid;
    private String name;
    private long price;

    public Good() {
        // TODO: What is the problem????
        // Problem Solved
        uid = String.valueOf(prevGoodUid.incrementAndGet());
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }
}
