package com.e_commerce.warehouse.service.impl;

import com.e_commerce.warehouse.domain.Good;
import com.e_commerce.warehouse.repository.GoodDAO;
import com.e_commerce.warehouse.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Service
public class DefaultWarehouseService implements WarehouseService {
    @Autowired
    private GoodDAO goodDAO;

    @Override
    public void addGood(Good good) {
        goodDAO.saveGood(good);
    }

    /**
     * TODO: specify what happens when no good with the given uid exists
     * */
    @Override
    public Good removeGood(String uid) {
        return goodDAO.removeGood(uid);
    }
}
