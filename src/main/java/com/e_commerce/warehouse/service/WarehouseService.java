package com.e_commerce.warehouse.service;

import com.e_commerce.warehouse.domain.Good;
import org.springframework.stereotype.Service;


public interface WarehouseService {
    void addGood(Good good);

    Good removeGood(String uid);
}
