package com.e_commerce.warehouse.repository;

import com.e_commerce.warehouse.domain.Good;

public interface GoodDAO {

    void saveGood(Good good);

    Good getGood(String uid);

    /**
     * @return the good with the given uid if it exists or null otherwise
     * */
    Good removeGood(String uid);
}
