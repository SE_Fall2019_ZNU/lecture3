package com.e_commerce.warehouse.repository.impl;

import com.e_commerce.warehouse.domain.Good;
import com.e_commerce.warehouse.repository.GoodDAO;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class InMemoryGoodDAO implements GoodDAO {
    Map<String, Good> uidToGoods = new HashMap<>();

    @Override
    public void saveGood(Good good) {
        uidToGoods.put(good.getUid(), good);
    }

    @Override
    public Good getGood(String uid) {
        return uidToGoods.get(uid);
    }

    @Override
    public Good removeGood(String uid) {
        return uidToGoods.remove(uid);
    }
}
