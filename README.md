# Spring Framework Introduction
> Two components of a simple shop to show the spring features(Sell & Warehouse) 

## Table of contents
* [Features](#general-info)
* [Further Reading](#further-reading)

## Features
* Spring Boot
* Spring Core (Dependency Injection, Several Annotations)

## Further Reading
* [Spring Boot Tuturial Video](https://www.youtube.com/watch?v=35EQXmHKZYs&t=6280s)
* [Spring in Action, Fifth Edition](https://www.manning.com/books/spring-in-action-fifth-edition)
